package org.gcube.portlets.admin.accountingmanager.client.maindata.charts.utils;

import com.github.highcharts4gwt.model.array.api.ArrayString;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class ColorsPalette {

	private String[] palette = { "#87CEEB", "#DAA520", "#3CB371", "#FF4500", "#8FBC8F", "#FF00FF", "#BDB76B", "#5F9EA0",
			"#A0522D", "#6B8E23", "#FFA07A", "#696969", "#DDA0DD", "#C71585", "#1E90FF", "#D2B48C", "#90EE90",
			"#B22222", "#00CED1", "#9400D3", "#FFDAB9", "#663399", "#FFE4C4", "#ADFF2F", "#FF0000", "#00FFFF",
			"#483D8B", "#FFFF00", "#00008B", "#FFDEAD", "#7CFC00", "#FF6347", "#AFEEEE", "#6A5ACD", "#FFFACD",
			"#0000CD", "#F5DEB3", "#00FF00", "#FF7F50", "#7FFFD4", "#7B68EE", "#FFEFD5", "#0000FF", "#DEB887",
			"#32CD32", "#FFA500", "#40E0D0", "#9370DB", "#FFE4B5", "#4169E1", "#BC8F8F", "#98FB98", "#8B0000",
			"#48D1CC", "#800080", "#EEE8AA", "#00BFFF", "#F4A460", "#00FA9A", "#800000", "#556B2F", "#8A2BE2",
			"#F0E68C", "#6495ED", "#B8860B", "#2E8B57", "#FFD700", "#66CDAA", "#9932CC", "#FFC0CB", "#87CEFA",
			"#CD853F", "#228B22", "#DC143C", "#20B2AA", "#BA55D3", "#FFB6C1", "#B0E0E6", "#D2691E", "#008000",
			"#CD5C5C", "#008B8B", "#FF00FF", "#FF69B4", "#B0C4DE", "#808000", "#006400", "#F08080", "#008080",
			"#EE82EE", "#FF1493", "#4682B4", "#8B4513", "#9ACD32", "#FA8072", "#778899", "#DA70D6", "#DB7093" };

	public ColorsPalette() {

	}

	public ArrayString getColorsPalette(ArrayString colors) {
		for (int i = 0; i < palette.length; i++) {
			colors.setValue(i, palette[i]);
		}
		colors.setLength(palette.length);
		return colors;
	}
}
