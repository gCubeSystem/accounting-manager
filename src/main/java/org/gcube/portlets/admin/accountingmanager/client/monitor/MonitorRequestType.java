package org.gcube.portlets.admin.accountingmanager.client.monitor;

/**
 * 
  * @author Giancarlo Panichi
 *
 *
 */
public enum MonitorRequestType {
	TimeOut, Period;
}
