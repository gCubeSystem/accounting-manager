package org.gcube.portlets.admin.accountingmanager.shared.data.response.service;

import java.util.ArrayList;

import org.gcube.portlets.admin.accountingmanager.shared.Constants;
import org.gcube.portlets.admin.accountingmanager.shared.data.ChartType;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SeriesServiceTop extends SeriesServiceDefinition {

	private static final long serialVersionUID = -2350334263342186590L;
	private Integer topNumber;
	private ArrayList<SeriesServiceDataTop> seriesServiceDataTopList;

	public SeriesServiceTop() {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = Constants.TOP_NUMBER_DEFAULT;

	}

	public SeriesServiceTop(Integer topNumber, ArrayList<SeriesServiceDataTop> seriesServiceDataTopList) {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = topNumber;
		this.seriesServiceDataTopList = seriesServiceDataTopList;
	}

	public Integer getTopNumber() {
		return topNumber;
	}

	public void setTopNumber(Integer topNumber) {
		this.topNumber = topNumber;
	}

	public ArrayList<SeriesServiceDataTop> getSeriesServiceDataTopList() {
		return seriesServiceDataTopList;
	}

	public void setSeriesServiceDataTopList(ArrayList<SeriesServiceDataTop> seriesServiceDataTopList) {
		this.seriesServiceDataTopList = seriesServiceDataTopList;
	}

	@Override
	public String toString() {
		return "SeriesServiceTop [topNumber=" + topNumber + ", seriesServiceDataTopList=" + seriesServiceDataTopList
				+ ", chartType=" + chartType + "]";
	}

}
