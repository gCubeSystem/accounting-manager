package org.gcube.portlets.admin.accountingmanager.shared.data;

import java.io.Serializable;
import java.util.ArrayList;

import org.gcube.portlets.admin.accountingmanager.shared.Constants;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class AccountingFilterTop extends AccountingFilterDefinition implements Serializable {

	private static final long serialVersionUID = -6805006183397381154L;
	private Context context;
	private String scopeFilterActive;
	private FilterKey filterKey;
	private Integer topNumber;
	private ArrayList<AccountingFilter> filters;

	public AccountingFilterTop() {
		super();
		this.chartType = ChartType.Top;
		context = null;
		scopeFilterActive = null;
		topNumber = Constants.TOP_NUMBER_DEFAULT;
		filterKey = null;
		filters = null;

	}

	public AccountingFilterTop(Context context, Integer topNumber) {
		super();
		this.chartType = ChartType.Top;
		this.context = context;
		this.scopeFilterActive = null;
		this.topNumber = topNumber;
		filterKey = null;
		filters = null;

	}

	public AccountingFilterTop(Context context, String scopeFilterActive, Integer topNumber) {
		super();
		this.chartType = ChartType.Top;
		this.context = context;
		this.scopeFilterActive = scopeFilterActive;
		this.topNumber = topNumber;
		filterKey = null;
		filters = null;

	}

	public AccountingFilterTop(Context context, FilterKey filterKey, ArrayList<AccountingFilter> filters,
			Integer topNumber) {
		super();
		chartType = ChartType.Top;
		this.context = context;
		this.scopeFilterActive = null;
		this.filterKey = filterKey;
		this.filters = filters;
		this.topNumber = topNumber;
	}

	public AccountingFilterTop(Context context, String scopeFilterActive, FilterKey filterKey,
			ArrayList<AccountingFilter> filters, Integer topNumber) {
		super();
		chartType = ChartType.Top;
		this.context = context;
		this.scopeFilterActive = scopeFilterActive;
		this.filterKey = filterKey;
		this.filters = filters;
		this.topNumber = topNumber;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public FilterKey getFilterKey() {
		return filterKey;
	}

	public void setFilterKey(FilterKey filterKey) {
		this.filterKey = filterKey;
	}

	public Integer getTopNumber() {
		return topNumber;
	}

	public void setTopNumber(Integer topNumber) {
		this.topNumber = topNumber;
	}

	public ArrayList<AccountingFilter> getFilters() {
		return filters;
	}

	public void setFilters(ArrayList<AccountingFilter> filters) {
		this.filters = filters;
	}

	public String getScopeFilterActive() {
		return scopeFilterActive;
	}

	public void setScopeFilterActive(String scopeFilterActive) {
		this.scopeFilterActive = scopeFilterActive;
	}

	@Override
	public String toString() {
		return "AccountingFilterTop [context=" + context + ", scopeFilterActive=" + scopeFilterActive + ", filterKey="
				+ filterKey + ", topNumber=" + topNumber + ", filters=" + filters + ", chartType=" + chartType + "]";
	}

}
