package org.gcube.portlets.admin.accountingmanager.server.amservice.command;

import java.util.ArrayList;
import java.util.SortedSet;

import org.gcube.accounting.analytics.NumberedFilter;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQuery;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQueryFactory;
import org.gcube.accounting.datamodel.aggregation.AggregatedStorageStatusRecord;
import org.gcube.portlets.admin.accountingmanager.shared.data.Spaces;
import org.gcube.portlets.admin.accountingmanager.shared.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class AccountingCommandGetSpaces implements AccountingCommand<Spaces> {
	private static final Logger logger = LoggerFactory.getLogger(AccountingCommandGetSpaces.class);

	public AccountingCommandGetSpaces() {
	}

	@Override
	public Spaces execute() throws ServiceException {
		try {
			logger.debug("getSpaces()");
			SortedSet<NumberedFilter> keys = null;
			AccountingPersistenceQuery apq = AccountingPersistenceQueryFactory.getInstance();
			
			//TODO the library must implement this interface
			keys = apq.getFilterValues(AggregatedStorageStatusRecord.DATA_TYPE);
			
			
			//keys = apq.getSpaceProvidersIds();

			logger.debug("AccountingPersistenceQuery.getSpaces: " + keys);
			ArrayList<String> spaceList = new ArrayList<>();

			if (keys == null || keys.isEmpty()) {
				logger.error("Spaces have not keys: {}", keys);
			} else {
				for (NumberedFilter key : keys) {
					if (key != null ) {
						spaceList.add(key.getValue());
					}
				}
			}
			Spaces categories = new Spaces(spaceList);

			logger.debug("Spaces:" + categories);

			return categories;
		} catch (Throwable e) {
			logger.error("Error in AccountingCommandGetSpaces(): {}", e.getLocalizedMessage(), e);
			throw new ServiceException("No spaces available!");

		}
	}

}
