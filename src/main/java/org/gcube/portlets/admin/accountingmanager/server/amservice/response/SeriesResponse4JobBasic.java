package org.gcube.portlets.admin.accountingmanager.server.amservice.response;

import java.util.ArrayList;
import java.util.List;

import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.datamodel.aggregation.AggregatedJobUsageRecord;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesJob;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobBasic;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobData;
import org.gcube.portlets.admin.accountingmanager.shared.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Series Response 4 Job Basic
 * 
   * @author Giancarlo Panichi
 *
 * 
 */
public class SeriesResponse4JobBasic extends SeriesResponseBuilder {
	protected static Logger logger = LoggerFactory
			.getLogger(SeriesResponse4JobBasic.class);
	private List<Info> infos;

	public SeriesResponse4JobBasic(List<Info> infos) {
		this.infos = infos;
	}

	@Override
	public void buildSeriesResponse() throws ServiceException {
		try {
			if (infos.size() <= 0) {
				logger.error("Error creating series for job accounting: No data available!");
				throw new ServiceException("No data available!");
			}

			ArrayList<SeriesJobData> series = new ArrayList<SeriesJobData>();
			for (Info info : infos) {
				JsonNode jso = info.getValue();
			
				Long duration = jso.get(AggregatedJobUsageRecord.DURATION).asLong();
				Long operationCount = jso
						.get(AggregatedJobUsageRecord.OPERATION_COUNT).asLong();
				Long maxInvocationTime = jso
						.get(AggregatedJobUsageRecord.MAX_INVOCATION_TIME).asLong();
				Long minInvocationTime = jso
						.get(AggregatedJobUsageRecord.MIN_INVOCATION_TIME).asLong();

				series.add(new SeriesJobData(info.getCalendar().getTime(),
						operationCount, duration, maxInvocationTime,
						minInvocationTime));

			}
			SeriesJobBasic seriesJobBasic = new SeriesJobBasic(series);

			SeriesJob seriesJob = new SeriesJob(seriesJobBasic);

			seriesResponseSpec.setSr(seriesJob);
		} catch (Throwable e) {
			logger.error("Error creating series for job accounting basic chart: "
					+ e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException(
					"Error creating series for job accounting basic chart: "
							+ e.getLocalizedMessage());
		}

	}
}
