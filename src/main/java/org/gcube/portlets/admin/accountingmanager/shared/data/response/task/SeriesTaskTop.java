package org.gcube.portlets.admin.accountingmanager.shared.data.response.task;

import java.util.ArrayList;

import org.gcube.portlets.admin.accountingmanager.shared.Constants;
import org.gcube.portlets.admin.accountingmanager.shared.data.ChartType;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SeriesTaskTop extends SeriesTaskDefinition {

	private static final long serialVersionUID = 6805210072384752359L;
	private Integer topNumber;
	private ArrayList<SeriesTaskDataTop> seriesTaskDataTopList;

	public SeriesTaskTop() {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = Constants.TOP_NUMBER_DEFAULT;

	}

	public SeriesTaskTop(Integer topNumber, ArrayList<SeriesTaskDataTop> seriesTaskDataTopList) {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = topNumber;
		this.seriesTaskDataTopList = seriesTaskDataTopList;
	}

	public Integer getTopNumber() {
		return topNumber;
	}

	public void setTopNumber(Integer topNumber) {
		this.topNumber = topNumber;
	}

	public ArrayList<SeriesTaskDataTop> getSeriesTaskDataTopList() {
		return seriesTaskDataTopList;
	}

	public void setSeriesTaskDataTopList(ArrayList<SeriesTaskDataTop> seriesTaskDataTopList) {
		this.seriesTaskDataTopList = seriesTaskDataTopList;
	}

	@Override
	public String toString() {
		return "SeriesTaskTop [topNumber=" + topNumber + ", seriesTaskDataTopList=" + seriesTaskDataTopList
				+ ", chartType=" + chartType + "]";
	}

}
