package org.gcube.portlets.admin.accountingmanager.shared.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class AccountingFilterBasic extends AccountingFilterDefinition implements Serializable {

	private static final long serialVersionUID = -6805006183397381154L;
	private ArrayList<AccountingFilter> filters;
	private Context context;
	private String scopeFilterActive;

	public AccountingFilterBasic() {
		super();
		this.chartType = ChartType.Basic;
		filters = null;
		context = null;
		scopeFilterActive = null;
	}

	public AccountingFilterBasic(Context context) {
		super();
		chartType = ChartType.Basic;
		this.filters = null;
		this.context = context;
		this.scopeFilterActive = null;
	}

	public AccountingFilterBasic(Context context,String scopeFilterActive) {
		super();
		chartType = ChartType.Basic;
		this.filters = null;
		this.context = context;
		this.scopeFilterActive = scopeFilterActive;
	}

	
	public AccountingFilterBasic(Context context, ArrayList<AccountingFilter> filters) {
		super();
		chartType = ChartType.Basic;
		this.filters = filters;
		this.context = context;
		this.scopeFilterActive = null;
	}

	public AccountingFilterBasic(Context context, String scopeFilterActive, ArrayList<AccountingFilter> filters) {
		super();
		chartType = ChartType.Basic;
		this.filters = filters;
		this.context = context;
		this.scopeFilterActive = scopeFilterActive;
	}
	
	public String getScopeFilterActive() {
		return scopeFilterActive;
	}

	public void setScopeFilterActive(String scopeFilterActive) {
		this.scopeFilterActive = scopeFilterActive;
	}

	public ArrayList<AccountingFilter> getFilters() {
		return filters;
	}

	public void setFilters(ArrayList<AccountingFilter> filters) {
		this.filters = filters;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public String toString() {
		return "AccountingFilterBasic [filters=" + filters + ", context=" + context + ", scopeFilterActive="
				+ scopeFilterActive + ", chartType=" + chartType + "]";
	}

	
}
