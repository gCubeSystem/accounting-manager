/**
 * 
 */
package org.gcube.portlets.admin.accountingmanager.shared.exception;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class NoScopeSelectedException extends Exception {

	private static final long serialVersionUID = -8737011216478988776L;

	/**
	 * 
	 */
	public NoScopeSelectedException() {
		super();
	}

	/**
	 * @param message
	 *            message
	 */
	public NoScopeSelectedException(String message) {
		super(message);
	}

	public NoScopeSelectedException(String message, Throwable t) {
		super(message, t);
	}

}
