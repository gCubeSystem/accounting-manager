package org.gcube.portlets.admin.accountingmanager.server.amservice.response;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.SortedMap;

import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.NumberedFilter;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.portlets.admin.accountingmanager.shared.data.FilterValue;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesJob;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobData;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobDataTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobTop;
import org.gcube.portlets.admin.accountingmanager.shared.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Top Series Response 4 Job
 * 
   * @author Giancarlo Panichi
 *
 * 
 */
public class SeriesResponse4JobTop extends SeriesResponseBuilder {
	protected static Logger logger = LoggerFactory
			.getLogger(SeriesResponse4JobTop.class);
	private Integer topNumber;
	private SortedMap<NumberedFilter, SortedMap<Calendar, Info>> topSM;

	public SeriesResponse4JobTop(Integer topNumber,
			SortedMap<NumberedFilter, SortedMap<Calendar, Info>> topSM) {
		this.topNumber = topNumber;
		this.topSM = topSM;
	}

	@Override
	public void buildSeriesResponse() throws ServiceException {
		try {
			if (topSM == null || topSM.isEmpty()) {
				logger.error("Error creating series for job accounting: No data available!");
				throw new ServiceException(
						"No data available!");
			}
			

			ArrayList<SeriesJobDataTop> seriesJobDataTopList = new ArrayList<>();

			for (NumberedFilter topValue : topSM.keySet()) {

				ArrayList<SeriesJobData> series = new ArrayList<>();
				SortedMap<Calendar, Info> infos = topSM.get(topValue);
				for (Info info : infos.values()) {
					JsonNode jso = info.getValue();
					Long duration = jso
							.get(AggregatedServiceUsageRecord.DURATION).asLong();
					Long operationCount = jso
							.get(AggregatedServiceUsageRecord.OPERATION_COUNT).asLong();
					Long maxInvocationTime = jso
							.get(AggregatedServiceUsageRecord.MAX_INVOCATION_TIME).asLong();
					Long minInvocationTime = jso
							.get(AggregatedServiceUsageRecord.MIN_INVOCATION_TIME).asLong();

					series.add(new SeriesJobData(info.getCalendar()
							.getTime(), operationCount, duration,
							maxInvocationTime, minInvocationTime));

				}
				SeriesJobDataTop seriesJobDataTop = new SeriesJobDataTop(
						new FilterValue(topValue.getValue()), series);
				seriesJobDataTopList.add(seriesJobDataTop);

			}

			SeriesJobTop seriesJobTop = new SeriesJobTop(topNumber,
					seriesJobDataTopList);
			SeriesJob seriesService = new SeriesJob(seriesJobTop);

			seriesResponseSpec.setSr(seriesService);

		} catch (Throwable e) {
			logger.error("Error creating series for job accounting top chart: "
					+ e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException(
					"Error creating series for job accounting top chart: "
							+ e.getLocalizedMessage());
		}

	}
}
