package org.gcube.portlets.admin.accountingmanager.server.amservice.response;

import java.util.ArrayList;
import java.util.List;

import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesService;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceBasic;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceData;
import org.gcube.portlets.admin.accountingmanager.shared.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Series Response 4 Service Basic
 * 
   * @author Giancarlo Panichi
 *
 * 
 */
public class SeriesResponse4ServiceBasic extends SeriesResponseBuilder {
	protected static Logger logger = LoggerFactory
			.getLogger(SeriesResponse4ServiceBasic.class);
	private List<Info> infos;

	public SeriesResponse4ServiceBasic(List<Info> infos) {
		this.infos = infos;
	}

	@Override
	public void buildSeriesResponse() throws ServiceException {
		try {
			if (infos.size() <= 0) {
				logger.error("Error creating series for service accounting: No data available!");
				throw new ServiceException("No data available!");
			}

			ArrayList<SeriesServiceData> series = new ArrayList<SeriesServiceData>();
			for (Info info : infos) {
				JsonNode jso = info.getValue();
				Long duration = jso
						.get(AggregatedServiceUsageRecord.DURATION).asLong();
				Long operationCount = jso
						.get(AggregatedServiceUsageRecord.OPERATION_COUNT).asLong();
				Long maxInvocationTime = jso
						.get(AggregatedServiceUsageRecord.MAX_INVOCATION_TIME).asLong();
				Long minInvocationTime = jso
						.get(AggregatedServiceUsageRecord.MIN_INVOCATION_TIME).asLong();

				series.add(new SeriesServiceData(info.getCalendar().getTime(),
						operationCount, duration, maxInvocationTime,
						minInvocationTime));

			}

			SeriesServiceBasic seriesServiceBasic = new SeriesServiceBasic(
					series);

			SeriesService seriesService = new SeriesService(seriesServiceBasic);

			seriesResponseSpec.setSr(seriesService);
		} catch (Throwable e) {
			logger.error("Error creating series for service accounting chart basic: "
					+ e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException(
					"Error creating series for service accounting chart basic: "
							+ e.getLocalizedMessage());
		}

	}
}
