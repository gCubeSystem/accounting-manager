package org.gcube.portlets.admin.accountingmanager.shared.data.response.job;

import java.util.ArrayList;

import org.gcube.portlets.admin.accountingmanager.shared.Constants;
import org.gcube.portlets.admin.accountingmanager.shared.data.ChartType;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SeriesJobTop extends SeriesJobDefinition {

	private static final long serialVersionUID = -2350334263342186590L;
	private ArrayList<SeriesJobDataTop> seriesJobDataTopList;
	private Integer topNumber;

	public SeriesJobTop() {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = Constants.TOP_NUMBER_DEFAULT;
	}

	public SeriesJobTop(Integer topNumber, ArrayList<SeriesJobDataTop> seriesJobDataTopList) {
		super();
		this.chartType = ChartType.Top;
		this.topNumber = topNumber;
		this.seriesJobDataTopList = seriesJobDataTopList;
	}

	public Integer getTopNumber() {
		return topNumber;
	}

	public void setTopNumber(Integer topNumber) {
		this.topNumber = topNumber;
	}

	public ArrayList<SeriesJobDataTop> getSeriesJobDataTopList() {
		return seriesJobDataTopList;
	}

	public void setSeriesJobDataTopList(ArrayList<SeriesJobDataTop> seriesJobDataTopList) {
		this.seriesJobDataTopList = seriesJobDataTopList;
	}

	@Override
	public String toString() {
		return "SeriesJobTop [seriesJobDataTopList=" + seriesJobDataTopList + ", topNumber=" + topNumber
				+ ", chartType=" + chartType + "]";
	}

}
