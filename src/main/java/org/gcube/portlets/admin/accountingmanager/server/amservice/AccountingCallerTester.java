package org.gcube.portlets.admin.accountingmanager.server.amservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.portlets.admin.accountingmanager.shared.data.AccountingFilterTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.AccountingType;
import org.gcube.portlets.admin.accountingmanager.shared.data.FilterKey;
import org.gcube.portlets.admin.accountingmanager.shared.data.FilterValue;
import org.gcube.portlets.admin.accountingmanager.shared.data.FilterValuesRequest;
import org.gcube.portlets.admin.accountingmanager.shared.data.FilterValuesResponse;
import org.gcube.portlets.admin.accountingmanager.shared.data.Spaces;
import org.gcube.portlets.admin.accountingmanager.shared.data.query.SeriesRequest;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesJob;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesResponse;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesService;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesSpace;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.SeriesStorage;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobBasic;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobData;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobDataTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobDefinition;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.job.SeriesJobTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceBasic;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceData;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceDataTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceDefinition;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.service.SeriesServiceTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.space.SeriesSpaceData;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.space.SeriesSpaceDataSpaces;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.space.SeriesSpaceDefinition;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.space.SeriesSpaceSpaces;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.storage.SeriesStorageBasic;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.storage.SeriesStorageData;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.storage.SeriesStorageDataTop;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.storage.SeriesStorageDefinition;
import org.gcube.portlets.admin.accountingmanager.shared.data.response.storage.SeriesStorageTop;
import org.gcube.portlets.admin.accountingmanager.shared.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class AccountingCallerTester implements AccountingCallerInterface {
	private static Logger logger = LoggerFactory.getLogger(AccountingCallerTester.class);

	protected SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMMMM dd");

	public AccountingCallerTester() {
		super();
	}

	public ArrayList<FilterKey> getFilterKeys(AccountingType accountingType) throws ServiceException {
		try {
			logger.debug("getFilterKeys(): [AccountingType=" + accountingType + "]");
			if (accountingType == null) {
				return new ArrayList<FilterKey>();
			}
			ArrayList<FilterKey> filterKeys = new ArrayList<FilterKey>();

			FilterKey key = new FilterKey("ConsumerId");
			filterKeys.add(key);

			key = new FilterKey("ClassName");
			filterKeys.add(key);

			for (int i = 0; i < 20; i++) {
				key = new FilterKey("ServiceName" + i);
				filterKeys.add(key);
			}

			logger.debug("List FilterKeys:" + filterKeys);
			return filterKeys;
		} catch (Throwable e) {
			logger.error("Error in getFilterKeys(): " + e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException("No keys available!");

		}
	}

	public FilterValuesResponse getFilterValues(FilterValuesRequest filterValuesRequest) throws ServiceException {
		try {
			logger.debug("getFilterValue(): [FilterValueRequest=" + filterValuesRequest + "]");
			FilterValuesResponse filterValuesResponse = null;
			if (filterValuesRequest == null || filterValuesRequest.getAccountingType() == null
					|| filterValuesRequest.getFilterKey() == null) {
				filterValuesResponse = new FilterValuesResponse(new ArrayList<FilterValue>());
				return filterValuesResponse;

			}

			ArrayList<FilterValue> filterValues = new ArrayList<FilterValue>();
			List<String> values;

			String[] vals = { "giancarlo.panichi", "gianpaolo.coro" };

			switch (filterValuesRequest.getAccountingType()) {
			case JOB:
			case PORTLET:
			case SERVICE:
			case STORAGE:
			case TASK:
			case SPACE:
			default:
				values = Arrays.asList(vals);
			}
			for (String value : values) {
				if (value != null && !value.isEmpty()) {
					filterValues.add(new FilterValue(value));
				}
			}

			filterValuesResponse = new FilterValuesResponse(filterValues);
			return filterValuesResponse;
		} catch (Throwable e) {
			logger.error("Error in getFilterValues(): " + e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException("No values available!");

		}
	}

	public SeriesResponse getSeries(AccountingType accountingType, SeriesRequest seriesRequest)
			throws ServiceException {
		try {
			logger.debug("getSeries(): [AccountingType=" + accountingType + " , seriesRequest=" + seriesRequest + "]");
			SeriesResponse seriesResponse = null;

			Calendar startCalendar = GregorianCalendar.getInstance(TemporalConstraint.DEFAULT_TIME_ZONE);

			try {
				startCalendar.setTime(sdf.parse(seriesRequest.getAccountingPeriod().getStartDate()));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ServiceException("Start Date not valid!");
			}

			Calendar endCalendar = GregorianCalendar.getInstance(TemporalConstraint.DEFAULT_TIME_ZONE);

			// GregorianCalendar endCalendar = new GregorianCalendar();
			// .getInstance(TemporalConstraint.DEFAULT_TIME_ZONE);

			// GregorianCalendar.getInstance(TemporalConstraint.DEFAULT_TIME_ZONE);
			try {
				endCalendar.setTime(sdf.parse(seriesRequest.getAccountingPeriod().getEndDate()));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ServiceException("End Date not valid!");
			}

			switch (accountingType) {
			case JOB:
				SeriesJobDefinition serieJobDefinition = null;
				if (seriesRequest.getAccountingFilterDefinition().getChartType() != null) {
					ArrayList<SeriesJobData> seriesJobData = new ArrayList<>();
					for (int i = 0; i < 30; i++) {
						SeriesJobData data = new SeriesJobData(new Date(), new Double(10 * Math.random()).longValue(),
								new Double(10 * Math.random()).longValue(), new Double(10 * Math.random()).longValue(),
								new Double(100 * Math.random()).longValue());
						seriesJobData.add(data);

					}
					switch (seriesRequest.getAccountingFilterDefinition().getChartType()) {
					case Basic:
						serieJobDefinition = new SeriesJobBasic(seriesJobData);
						break;
					case Top:
						AccountingFilterTop accountingFilterTop = (AccountingFilterTop) seriesRequest
								.getAccountingFilterDefinition();

						ArrayList<SeriesJobDataTop> seriesJobDataTopList = new ArrayList<>();
						for (int i = 0; i < accountingFilterTop.getTopNumber(); i++) {

							SeriesJobDataTop seriesJobDataTop = new SeriesJobDataTop(new FilterValue("User" + i),
									seriesJobData);
							seriesJobDataTopList.add(seriesJobDataTop);
						}
						serieJobDefinition = new SeriesJobTop(accountingFilterTop.getTopNumber(), seriesJobDataTopList);
						break;

					default:
						break;

					}
				}

				seriesResponse = new SeriesJob(serieJobDefinition);

				break;
			case PORTLET:
				break;
			case SERVICE:
				SeriesServiceDefinition serieServiceDefinition = null;
				if (seriesRequest.getAccountingFilterDefinition().getChartType() != null) {
					ArrayList<SeriesServiceData> seriesServiceData = new ArrayList<>();
					switch (seriesRequest.getAccountingPeriod().getPeriod()) {
					case DAILY:
					case HOURLY:
					case MINUTELY:
						for (int i = 0; i < 30; i++) {
							SeriesServiceData data = new SeriesServiceData(new Date(),
									new Double(50 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue());
							seriesServiceData.add(data);
						}
						break;
					case MONTHLY:
						while (startCalendar.compareTo(endCalendar) <= 0) {
							SeriesServiceData data = new SeriesServiceData(startCalendar.getTime(),
									new Double(50 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue());
							seriesServiceData.add(data);
							startCalendar.add(Calendar.MONTH, 1);
						}
						break;
					case YEARLY:
						while (startCalendar.compareTo(endCalendar) <= 0) {
							SeriesServiceData data = new SeriesServiceData(startCalendar.getTime(),
									new Double(50 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue(),
									new Double(10000 * Math.random()).longValue());
							seriesServiceData.add(data);
							startCalendar.add(Calendar.YEAR, 1);
						}
						break;
					default:
						break;

					}

					switch (seriesRequest.getAccountingFilterDefinition().getChartType()) {
					case Basic:
						serieServiceDefinition = new SeriesServiceBasic(seriesServiceData);
						break;
					case Top:
						AccountingFilterTop accountingFilterTop = (AccountingFilterTop) seriesRequest
								.getAccountingFilterDefinition();
						ArrayList<SeriesServiceDataTop> seriesServiceDataTopList = new ArrayList<>();

						for (int i = 0; i < accountingFilterTop.getTopNumber(); i++) {

							SeriesServiceDataTop seriesServiceDataTop1 = new SeriesServiceDataTop(
									new FilterValue("User" + i), seriesServiceData);
							seriesServiceDataTopList.add(seriesServiceDataTop1);
						}

						serieServiceDefinition = new SeriesServiceTop(accountingFilterTop.getTopNumber(),
								seriesServiceDataTopList);
						break;

					default:
						break;

					}
				}

				seriesResponse = new SeriesService(serieServiceDefinition);

				break;
			case SPACE:
				SeriesSpaceDefinition serieSpaceDefinition = null;
				if (seriesRequest.getAccountingFilterDefinition().getChartType() != null) {
					ArrayList<SeriesSpaceData> seriesSpaceData = new ArrayList<>();
					switch (seriesRequest.getAccountingPeriod().getPeriod()) {
					case DAILY:
					case HOURLY:
					case MINUTELY:
						for (int i = 0; i < 30; i++) {
							SeriesSpaceData data = new SeriesSpaceData(new Date(),
									new Double(10000 * Math.random()).longValue());
							seriesSpaceData.add(data);
						}
						break;
					case MONTHLY:
						while (startCalendar.compareTo(endCalendar) <= 0) {
							SeriesSpaceData data = new SeriesSpaceData(startCalendar.getTime(),
									new Double(10000 * Math.random()).longValue());
							seriesSpaceData.add(data);
							startCalendar.add(Calendar.MONTH, 1);
						}
						break;
					case YEARLY:
						while (startCalendar.compareTo(endCalendar) <= 0) {
							SeriesSpaceData data = new SeriesSpaceData(startCalendar.getTime(),
									new Double(10000 * Math.random()).longValue());
							seriesSpaceData.add(data);
							startCalendar.add(Calendar.YEAR, 1);
						}
						break;
					default:
						break;

					}

					switch (seriesRequest.getAccountingFilterDefinition().getChartType()) {
					case Basic:
					case Top:
					case Spaces:
						// AccountingFilterContext accountingFilterContext =
						// (AccountingFilterContext) seriesRequest
						// .getAccountingFilterDefinition();
						ArrayList<SeriesSpaceDataSpaces> seriesSpaceDataSpacesList = new ArrayList<>();
						ArrayList<String> spacesList = new ArrayList<>();

						for (int i = 1; i < 3; i++) {
							String space = new String("Space" + i);
							spacesList.add(space);
							SeriesSpaceDataSpaces seriesSpaceDataSpaces = new SeriesSpaceDataSpaces(space,
									seriesSpaceData);
							seriesSpaceDataSpacesList.add(seriesSpaceDataSpaces);
						}

						Spaces spaces = new Spaces(spacesList);
						serieSpaceDefinition = new SeriesSpaceSpaces(spaces, seriesSpaceDataSpacesList);

					default:
						break;

					}
				}

				seriesResponse = new SeriesSpace(serieSpaceDefinition);

				break;

			case STORAGE:
				SeriesStorageDefinition serieStorageDefinition = null;
				if (seriesRequest.getAccountingFilterDefinition().getChartType() != null) {
					ArrayList<SeriesStorageData> seriesStorageData = new ArrayList<>();
					for (int i = 0; i < 30; i++) {

						// Valori variabili
						SeriesStorageData data = new SeriesStorageData(new Date(),
								new Double(1024 * 1024 * Math.random()).longValue(),
								new Double(100 * Math.random()).longValue());

						// Valori Fissi
						/*
						 * SeriesStorageData data = new SeriesStorageData( new
						 * Date(), new Double(1024 * 1024) .longValue(), new
						 * Double( 100).longValue());
						 */

						seriesStorageData.add(data);

					}
					switch (seriesRequest.getAccountingFilterDefinition().getChartType()) {
					case Basic:
						for (SeriesStorageData serieStorageData : seriesStorageData) {
							serieStorageData.setDataVolume(serieStorageData.getDataVolume() * 1000);
							serieStorageData.setOperationCount(serieStorageData.getOperationCount() * 1000);
						}

						serieStorageDefinition = new SeriesStorageBasic(seriesStorageData);
						break;
					case Top:
						AccountingFilterTop accountingFilterTop = (AccountingFilterTop) seriesRequest
								.getAccountingFilterDefinition();
						ArrayList<SeriesStorageDataTop> seriesStorageDataTopList = new ArrayList<>();

						for (int i = 0; i < accountingFilterTop.getTopNumber(); i++) {
							SeriesStorageDataTop seriesStorageDataTop = new SeriesStorageDataTop(
									new FilterValue("User" + i), seriesStorageData);
							seriesStorageDataTopList.add(seriesStorageDataTop);
						}

						serieStorageDefinition = new SeriesStorageTop(accountingFilterTop.getTopNumber(),
								seriesStorageDataTopList);
						break;
					default:
						break;

					}
				}

				seriesResponse = new SeriesStorage(serieStorageDefinition);

				break;
			case TASK:
				break;
			default:
				break;

			}

			if (seriesResponse == null) {
				throw new ServiceException("Error creating series response!");
			}
			logger.debug("SeriesResponse Created: " + seriesResponse);
			return seriesResponse;
		} catch (Throwable e) {
			logger.error("Error in GetSeries(): " + e.getLocalizedMessage());
			e.printStackTrace();
			throw new ServiceException("No data available!");

		}
	}

	@Override
	public Spaces getSpaces() throws ServiceException {
		ArrayList<String> categoriesList = new ArrayList<>();
		categoriesList.add("Space1");
		categoriesList.add("Space2");
		Spaces categories = new Spaces(categoriesList);
		return categories;
	}
}
