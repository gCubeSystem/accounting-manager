# Accounting Manager

Accounting Manager is a administration tool for monitoring and displaying statistical data concerning the D4Science Infrastructure.

## Structure of the project

* The source code is present in the src folder. 

![Accounting Manager](./images/Accounting_manager.png)

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* Use of this portlet is is described on [Wiki](https://wiki.gcube-system.org/gcube/Accounting_Portlet).

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/accounting-manager/releases).

## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [CNR-ISTI Infrascience Group](https://infrascience.isti.cnr.it/)


## License

This project is licensed under the license **EUPL V. 1.2** - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)
