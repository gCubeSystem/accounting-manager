This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "accounting-manager"


## [v1.16.0]

-  Updated to maven-portal-bom 4.0.0


## [v1.15.0] - 2021-11-30

-  Improve query support of the new persistence [#21354]


## [v1.14.0] - 2021-06-28

- Fixed display errors generated in Root scope [#21729]


## [v1.13.0] - 2021-03-23

- Migrate accounting-manager to postgresql persistence [#21013]


## [v1.12.0] - 2019-05-02

- Added autocomplete combo scope and fixed scope list view width


## [v1.11.0] - 2018-12-28

- Added scope selection [#10188]


## [v1.10.0] - 2018-10-01

- Updated to StorageHub [#12474]


## [v1.9.1] - 2018-04-11

- Updated log level to debug [#11259]


## [v1.9.0] - 2017-11-20

- Updated to support Operation Count as default in all charts


## [v1.8.0] - 2017-10-01

- Updated to new version of accounting client library


## [v1.7.0] - 2017-06-12

- Added Spaces chart [#8397]


## [v1.6.0] - 2017-02-28

- Fixed download in case of multitab on different scopes


## [v1.5.0] - 2016-12-01

- Added Context Chart
- Added Cache
- Updated to PortalContext


## [v1.4.0] - 2016-10-01

- Updated to AUTH2.0
- Added Job
- Added configuration by generic resource


## [v1.3.0] - 2016-07-15

- Added CSV download [Ticket #4041]
- Added Show Others option
- Updated to Liferay 6.2


## [v1.2.0] - 2016-05-01

- Added Top N Chart[#2251]
- Fixed back button behavior[#3251]


## [v1.1.0] - 2016-01-31

- Added to Storage Accounting more formats for DataVolume axis [#1831]
- Fixed on Storage Accounting the popup validation on filters [#1832]
- Fixed library dependencies


## [v1.0.0] - 2015-10-15

- First Release

